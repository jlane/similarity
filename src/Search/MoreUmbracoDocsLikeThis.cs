﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Similarity.Model;
using Umbraco.Core;
using Umbraco.Core.IO;
using Examine.LuceneEngine.Config;

namespace Similarity.Search
{
    /// <summary>
    /// wrapper class around lucene contrib similarity to get document like given document
    /// using specified fields to test for similarity
    /// </summary>
    public class MoreUmbracoDocsLikeThis : IDisposable
    {
        private readonly int _docId;
        private readonly int _maxNo;
        private readonly IEnumerable<string> _fieldsToSearch;
        private readonly IEnumerable<string> _docTypes;
        private readonly FSDirectory _directory;
        private readonly IndexReader _reader;
        private readonly IndexSearcher _searcher;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoreUmbracoDocsLikeThis"/> class.
        /// </summary>
        /// <param name="currentDocId">The doc id.</param>
        /// <param name="searchIndex">The index to search.</param>
        /// <param name="maximumResultsToReturn">The max no.</param>
        /// <param name="fields">The fields to search.</param>
        /// <param name="docTypesToSearchOn"></param>
        public MoreUmbracoDocsLikeThis(int currentDocId, string searchIndex, int maximumResultsToReturn,
                                       IEnumerable<string> fields, IEnumerable<string> docTypesToSearchOn)
        {
            _docId = currentDocId;
            _maxNo = maximumResultsToReturn;
            _fieldsToSearch = fields;
            _docTypes = docTypesToSearchOn;

            try
            {
                _directory = FSDirectory.Open(new DirectoryInfo(GetIndexPath(searchIndex)));
                _reader = IndexReader.Open(_directory, true);
                _searcher = new IndexSearcher(_reader);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        /// <summary>
        /// for given document and fields in that doc get fixed no of docs that are similar
        /// assumes you have index that is up to date
        /// </summary>
        /// <returns>list of similar docs found</returns>
        public IEnumerable<SearchResultItem> FindMoreLikeThis(out string generatedQuery)
        {
            var results = new List<SearchResultItem>();

            generatedQuery = string.Empty;

            if (IsInit())
            {
                var moreLikeThis = new MoreLikeThis(_reader);

                moreLikeThis.SetFieldNames(_fieldsToSearch.ToArray());
                moreLikeThis.SetMinTermFreq(1);
                moreLikeThis.SetMinDocFreq(1);

                int currentLuceneDocId = GetLuceneDocNo(_docId);

                if (currentLuceneDocId != 0)
                {
                    var query = moreLikeThis.Like(currentLuceneDocId);

                    var docTypeQuery = new BooleanQuery();

                    if (_docTypes.Any())
                    {
                        docTypeQuery = AddDocTypeFilter();
                        ((BooleanQuery)query).Add(docTypeQuery, BooleanClause.Occur.MUST);
                    }

                    generatedQuery = query.ToString();

                    var docs = _searcher.Search(query, _maxNo);

                    int count = docs.ScoreDocs.Length;
                    //start at 1 as first item will be current document itself which we dont want
                    for (int i = 1; i < count; i++)
                    {

                        var d = _reader.Document(docs.ScoreDocs[i].doc);
                        var item = new SearchResultItem
                        {
                            PageName = d.GetField("nodeName").StringValue(),
                            NodeId = int.Parse(d.GetField("__NodeId").StringValue()),
                            NodeTypeAlias = d.GetField("nodeTypeAlias").StringValue(),
                            PathIds = d.GetField("path").StringValue(),
                            Path = GetFullPath(d.GetField("path").StringValue()),
                            RelevanceScore = docs.ScoreDocs[i].score,
                            RelevancePercentage = (docs.ScoreDocs[i].score * 100).ToString("##")
                        };
                        results.Add(item);
                    }
                }
            }
            return results;

        }

        private string GetFullPath(string ids)
        {
            var sb = new StringBuilder();

            var docPathIds = ids.Split(',').Where(s => s != "-1").ToList();

            foreach (var id in docPathIds)
            {
                var document = ApplicationContext.Current.Services.ContentService.GetById(int.Parse(id));
                sb.Append(document.Name);
                sb.Append(" > ");
            }

            return sb.ToString().TrimEnd(" > ");
        }

        /// <summary>
        /// only want specific doc types
        /// </summary>        
        /// <returns></returns>
        private BooleanQuery AddDocTypeFilter()
        {
            var docTypeQueryInner = new BooleanQuery();

            foreach (var docType in _docTypes)
            {
                var docTypeQuery = new TermQuery(new Term("nodeTypeAlias", docType.ToLower()));
                docTypeQueryInner.Add(docTypeQuery, BooleanClause.Occur.SHOULD);
            }

            return docTypeQueryInner;
        }

        private bool IsInit()
        {
            if (_directory != null && _reader != null && _searcher != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// from the current umbraco node id we need actual lucene index unique id to query on
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        private int GetLuceneDocNo(int docId)
        {
            var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "__NodeId",
                new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29));
            var query = parser.Parse(docId.ToString());
            var doc = _searcher.Search(query, 1);
            if (doc.TotalHits == 0)
            {
                return 0;
            }
            return doc.ScoreDocs[0].doc;
        }

        /// <summary>
        /// get path to lucene index for lucene to query on, probably better way using examine api
        /// but couldnt figure it out
        /// </summary>
        /// <param name="indexToQuery"></param>
        /// <returns></returns>
        private string GetIndexPath(string indexToQuery)
        {
            IndexSetCollection sets = Examine.LuceneEngine.Config.IndexSets.Instance.Sets;
            IndexSet set = sets["ExternalIndexSet"];
            DirectoryInfo dir = set.IndexDirectory;
            return Path.Combine(dir.FullName, "Index");
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (_directory != null)
            {
                _directory.Close();
            }

            if (_reader != null)
            {
                _reader.Close();
            }

            if (_searcher != null)
            {
                _searcher.Close();
            }
        }
    }
}
