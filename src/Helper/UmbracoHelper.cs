﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;

namespace Similarity.Helper
{
    public static class UmbracoHelper
    {
        public static IEnumerable<string> GetDocumentTypes(List<string> ignore)
        {
            var docTypes = new List<string>();

            if (ignore.Any())
            {
                var cmsDocTypes = ApplicationContext.Current.Services.ContentTypeService.GetAllContentTypes();

                foreach (var documentType in cmsDocTypes)
                {
                    if (ignore.Contains(documentType.Alias))
                    {
                        docTypes.Add(documentType.Alias);
                    }
                }
            }

            return docTypes;
        }
    }
}
