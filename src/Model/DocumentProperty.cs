﻿using System.Runtime.Serialization;

namespace Similarity.Model
{
    [DataContract(Name = "documentProperty")]
    public class DocumentProperty
    {
        [DataMember(Name = "propertyAlias")]
        public string PropertyAlias { get; set; }

        [DataMember(Name = "alias")]
        public string Alias { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }
    }
}
