﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Similarity.Model
{
    [DataContract(Name = "searchResults")]
    public class SimilarSearchResult
    {
        [DataMember(Name = "generatedQuery")]
        public string GeneratedQuery { get; set; }

        [DataMember(Name = "queryExecutionTime")]
        public string TimeTake { get; set; }

        [DataMember(Name = "results")]
        public IEnumerable<SearchResultItem> Results { get; set; } 
    }

    [DataContract(Name = "searchResult")]
    public class SearchResultItem
    {
        [DataMember(Name = "nodeId")]
        public int NodeId { get; set; }

        [DataMember(Name = "pageName")]
        public string PageName { get; set; }

        [DataMember(Name = "nodeTypeAlias")]
        public string NodeTypeAlias { get; set; }

        [DataMember(Name = "relevanceScore")]
        public float RelevanceScore { get; set; }

        [DataMember(Name = "relevancePercentage")]
        public string RelevancePercentage { get; set; }

        [DataMember(Name = "pathIds")]
        public string PathIds { get; set; }

        [DataMember(Name = "path")]
        public string Path { get; set; }

    }
}
