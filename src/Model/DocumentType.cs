﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Similarity.Model
{
    [DataContract(Name = "documentType")]
    public class DocumentType
    {
        [DataMember(Name = "documentTypeId")]
        public int DocumentTypeId { get; set; }

        [DataMember(Name = "documentTypeAlias")]
        public string DocumentTypeAlias { get; set; }

        [DataMember(Name = "properties")]
        public IEnumerable<DocumentProperty> Properties { get; set; } 
    }
}
