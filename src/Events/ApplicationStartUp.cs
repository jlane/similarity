﻿using System.Web.Http;
using System.Web.Routing;
using Umbraco.Core;

namespace Similarity.Events
{
    public class ApplicationStartUp : IApplicationEventHandler
    {

        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            MapWebApiRoutes();
        }

        private void MapWebApiRoutes()
        {
            RouteTable.Routes.MapHttpRoute("SimilarityApi", "umbraco/backoffice/api/SimilarityApi/{action}/{docId}/{indexToSearch}/{maxResults}/{docTypes}/{docTypeProperties}",
                                                        new
                                                        {
                                                            controller = "SimilarityApi"
                                                        }
                                         );
            
        }
    }
}
