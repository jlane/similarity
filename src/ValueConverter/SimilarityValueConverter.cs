﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Logging;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web;

namespace Similarity.ValueConverter
{
    [PropertyValueType(typeof(Similar))]
    [PropertyValueCache(PropertyCacheValue.All, PropertyCacheLevel.Content)]
    public class SimilarityValueConverter : PropertyValueConverterBase
    {
        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("Similarity");
        }

        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source == null) return null;
            var u = new UmbracoHelper(UmbracoContext.Current);

            var sourceString = source.ToString();

            List<string> docIds = sourceString.Split(',').ToList();

            var similarDocs = new Similar();

            try
            {
                foreach (var id in docIds)
                {
                    var document = u.TypedContent(id);
                    if (document == null) continue;
                    similarDocs.SimilarDocs.Add(document);
                }
                return similarDocs;
            }
            catch (Exception ex)
            {
                LogHelper.Error<Exception>("Error converting " + sourceString + " to SimilarityValueConverter", ex);
            }

            return sourceString;

        }
    }
}
