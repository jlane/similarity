﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace Similarity.ValueConverter
{
    public class Similar
    {
        public List<IPublishedContent> SimilarDocs { get; set; }

        public Similar()
        {
            SimilarDocs = new List<IPublishedContent>();
        }
    }
}
