﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http.ModelBinding;
using Examine;
using Examine.Providers;
using Newtonsoft.Json;
using Similarity.Helper;
using Similarity.Model;
using Similarity.Search;
using Umbraco.Web.WebApi;

namespace Similarity
{    
    public class SimilarityApiController : UmbracoAuthorizedApiController
    {

        /// <summary>
        /// for a given document, index, doc types to search and fields to search 
        /// get back similar documents
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="indexToSearch"></param>
        /// <param name="maxResults"></param>
        /// <param name="docTypes"></param>
        /// <param name="docTypeProperties"></param>
        /// <returns></returns>
        public SimilarSearchResult GetSimilarDocs(int docId, string indexToSearch, int maxResults,
                                                 string docTypes,
                                                 string docTypeProperties)
        {

            Stopwatch timer = Stopwatch.StartNew();

            IEnumerable<SearchResultItem> results = new List<SearchResultItem>();

            var similarSearchResult = new SimilarSearchResult();
            
            string generatedQuery = string.Empty;

            //we have csv list of ids we need csv list of aliases
            var documentTypes = UmbracoHelper.GetDocumentTypes(docTypes.Split(',').ToList());

            using (var moreLikeThis = new MoreUmbracoDocsLikeThis(docId, indexToSearch, maxResults, docTypeProperties.Split(','), documentTypes))
            {
                results = moreLikeThis.FindMoreLikeThis(out generatedQuery);
            }

            timer.Stop();

            TimeSpan timespan = timer.Elapsed;

            similarSearchResult.TimeTake = "(mm:ss:fff) " + timespan.ToString("mm':'ss':'fff");

            similarSearchResult.GeneratedQuery = generatedQuery;

            similarSearchResult.Results = results;

            return similarSearchResult;
        }


        public IEnumerable<DocumentType> GetDocumentTypes()
        {
            var docTypes=new List<DocumentType>();

            var cmsDocTypes = ApplicationContext.Services.ContentTypeService.GetAllContentTypes();

            foreach (var documentType in cmsDocTypes)
            {
                var d = new DocumentType
                {
                    DocumentTypeId = documentType.Id,
                    DocumentTypeAlias = documentType.Alias
                };

                AddProperties(d, documentType);

                docTypes.Add(d);
            }

            return docTypes.OrderBy(d=>d.DocumentTypeAlias);
        }

        public IEnumerable<string> GetIndexes()
        {
            IndexProviderCollection indexes = ExamineManager.Instance.IndexProviderCollection;
            var indexesForView = new List<string>();

            for(int i=0;i<indexes.Count;i++)
            {
                indexesForView.Add(indexes[i].Name);
            }

            return indexesForView;
        }

        private void AddProperties(DocumentType d, Umbraco.Core.Models.IContentType documentType)
        {
            var propertyTypes = documentType.PropertyTypes;

            var properties = propertyTypes.Select(property => new DocumentProperty
            {
                Alias = property.Alias, PropertyAlias = property.PropertyEditorAlias, Id = property.Id
            }).ToList();

            d.Properties = properties;
        }
    }

    /// <summary>
    /// model binder so we can pass into webapi ['fff','fgg'] array and convert it to list<string>
    /// </summary>
    public class JsonArrayStringModelBinder : IModelBinder
    {
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var key = bindingContext.ModelName;
            var val = bindingContext.ValueProvider.GetValue(key);
            if (val != null)
            {
                var s = val.AttemptedValue;
                
                if (s != null && IsJsonArray(s))
                {
                    var stringArray = JsonConvert.DeserializeObject<List<string>>(s);
                    bindingContext.Model = stringArray;
                }
                else
                {
                    bindingContext.Model = new[] { s };
                }
                return true;
            }
            return false;
        }

        private static bool IsJsonArray(string s)
        {
            return s.IndexOf('[',0)!=-1;
        }
    }
}
