//adds the resource to umbraco.resources module:
angular.module('umbraco.services').factory('sharedService', 
    function($rootScope,docTypesResource) {
            
            var sharedService = {};
            
		    // expose service data to angular scope
		    $rootScope.sharedData = sharedService.data = {};
		    
			//all doc types returned from webapi
            docTypesResource.getAllDocTypes().then(function(response){                        
                	sharedService.data.docTypes = response.data;
                	$rootScope.$broadcast('handleDocTypesLoaded');   
            });		    
			
			//currently selected doctypes in the select
			sharedService.data.selectedDocTypes=[];

		    return sharedService;
    }
); 