//adds the resource to umbraco.resources module:
angular.module('umbraco.resources').factory('indexResource', 
    function($q, $http) {
        //the factory object returned
        return {
            //this cals the Api Controller we setup earlier
            getAllIndexes: function () {
                return  $http.get("/umbraco/backoffice/api/SimilarityApi/GetIndexes");
            }
        };
    }
); 
angular.module('umbraco.resources').factory('docTypesResource', 
    function($q, $http) {
        //the factory object returned
        return {
            //this cals the Api Controller we setup earlier todo cache ??
            getAllDocTypes: function () {
                return  $http.get("/umbraco/backoffice/api/SimilarityApi/GetDocumentTypes");
            }
        };
    }
);
angular.module('umbraco.resources').factory('searchResource', 
    function($q, $http) {
        return {
            getSimilarDocs: function (currentDocId,indexToSearch,maxNo,docTypes,docProperties) {
                return  $http.get("/umbraco/backoffice/api/SimilarityApi/GetSimilarDocs",                                    
                                              {
                                                params: 
                                                { 
                                                    docId: currentDocId,
                                                    indexToSearch:indexToSearch,
                                                    maxResults:maxNo,
                                                    docTypes:docTypes,
                                                    docTypeProperties:docProperties 
                                                }
                                               }
                                  );
            }
        };
    }
);
