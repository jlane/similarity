﻿angular.module("umbraco").controller("similarity.indexController",
        function ($scope, indexResource) {
            
            //get the indexes from /umbraco/backoffice/api/SimilarityApi/GetIndexes
            indexResource.getAllIndexes().then(function(response){

                $scope.indexes= response.data;

            })
        });