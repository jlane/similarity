﻿angular.module("umbraco").controller("Similarity.DocTypeController",
        function ($scope,sharedService) {

			if(sharedService.data.docTypes===undefined){
				//this is initial load
				//sharedService.js raises event when doctypes retrieved from webapi
			   $scope.$on('handleDocTypesLoaded', function () {
			        $scope.docTypes = sharedService.data.docTypes; 
			    });
			}
			else{
				//we already have all doc types load and this
				//is refresh after a save
				$scope.docTypes = sharedService.data.docTypes; 			
			}

			$scope.docTypeChange = function(items){
				//set to shared object which is being watched in docTypeProperties.controller.js                	                	
				sharedService.data.selectedDocTypes=items;                	
			}

        });