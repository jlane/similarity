angular.module("umbraco").controller("Similarity.DocTypePropertiesController",
        function ($scope,$rootScope,sharedService,editorState){
 
    		/*
			 for each selected doctype get its properties				
			*/
			function processSelected(docTypeId){
				var selectedDocType = _.where(sharedService.data.docTypes,{documentTypeId:docTypeId})[0];	           	            	
				documentTypeProperties.push.apply(documentTypeProperties, selectedDocType.properties);
			}

			//list of all document type properties for selected doc types
			//this will be used in ng-options in the view
			var documentTypeProperties=[]; 
		   
			$scope.docTypeProperties =[];

			$scope.$on('handleDocTypesLoaded', function () {
					//inital load
				//do we already have previously saved doc types
				var currentState = editorState.current;				
				var currentDocTypes = _.where(currentState.preValues,{key:'docTypes'})[0];
			    
				if(currentDocTypes!==undefined){
								
					_.each(currentDocTypes.value.selectedDocTypes,processSelected);
				   
				   	$scope.docTypeProperties = documentTypeProperties;

				}

			});

			//handle the event change broadcast from doctypes.controller
            $rootScope.$watch('sharedData.selectedDocTypes', function() {
				documentTypeProperties=[]; 
		       	_.each(sharedService.data.selectedDocTypes,processSelected);		       
		       	$scope.docTypeProperties = documentTypeProperties;
		    
		    });
            
        });