﻿angular.module("umbraco").controller("Similarity.SearchController",
        function ($scope, $http,searchResource,$routeParams,docTypesResource,umbRequestHelper,entityResource,iconHelper) {

            $scope.loading = false;

            var allDocTypes;

            var displayResults =[]; //bind to this in the view

            $scope.results = [];

            $scope.selectResult = function(result){
                $scope.add(result);
            };

            function convertDocTypesToStringArray(docTypes){
                
                var stringArray=[];

                for(var i=0;i<docTypes.length;i++){
                    
                    var documentType = _.where(allDocTypes,{documentTypeId:docTypes[i]})[0];

                    properties.push.apply(properties, documentType.properties);
                                        
                    stringArray.push(documentType.documentTypeAlias);
                }
                
                return stringArray.toString();                     
            }

            function convertPropertiesToStringArray(docTypeProperties){
            
                var stringArray=[];
                for(var i=0;i<docTypeProperties.length;i++){
                    var docTypePropertyAlias = _.where(properties,{id:docTypeProperties[i]})[0];
                    stringArray.push(docTypePropertyAlias.alias);
                }   

                return stringArray.toString();         
            }

            function configureContentResult(content) {
                        
                        content.menuUrl = umbRequestHelper.getApiUrl("contentTreeBaseUrl", 
                                                                     "GetMenu", 
                                                                     [{ id: content.id }, 
                                                                     { application: 'content' }]);
                        
                        content.editorPath = "content/content/edit/" + content.id;
                        content.metaData = { treeAlias: "content" };
                        content.subTitle = content.metaData.Url;
                        content.icon = iconHelper.convertFromLegacyIcon(content.icon);
                        return content;        
            }

            function doSearch(){
                        var searchDocTypes = convertDocTypesToStringArray(options.docTypes.selectedDocTypes);
                        var indexFields = convertPropertiesToStringArray(options.docTypeProperties.selectedDocTypeProperties);
                        if(indexFields != '') {
                            indexFields += ',';
                        }
                        indexFields += options.indexFields;
                        searchResource.getSimilarDocs(currentDocId,options.index.index,
                                                    options.maxNo,
                                                    searchDocTypes,
                                                    indexFields)
                                                    .then(function(response){

                                                            var searchResults = response.data.results;

                                                            for(var i=0;i<searchResults.length;i++){
                                                                //self executing so we can inject in the doc path      
                                                                (function(iterator){
                                                                    var id = searchResults[iterator].nodeId;
                                                                    var path = searchResults[iterator].path;
                                                                    entityResource.getById(id, "Document")
                                                                           .then(function(ent) {
                                                                               var content = ent;
                                                                               content.displayPath = path;                                                                                                                                                                                                                                                                                                           
                                                                               displayResults.push(configureContentResult(content));
                                                                           });
                                                                })(i);
                                                            
                                                            }

                                                            $scope.results = displayResults; 
                                                            $scope.loading=false;
                                                            
                                                    });
            }

            var currentDocId = $routeParams.id;      
            
            entityResource.getById(currentDocId, "Document")
                   .then(function(ent) {
                       $scope.currentDocName = ent.name;                                                                                                                                                                              
                   }); 

            var properties=[];

            var options=$scope.model.config;
            
            //need the doctypes to do lookups from the config object
            //so we can do the search
            docTypesResource.getAllDocTypes().then(function(response){      
                    $scope.loading=true;       
                    
        	        allDocTypes = response.data;
                    //this method builds up displayResults array
                    doSearch();                    
            });	

        });