﻿angular.module("umbraco").controller("Similarity.EditorController",
        function ($scope, editorState) {            
			if(!$scope.model.value){
				$scope.model.value = {};
				//populate with values from the editor
				$scope.model.value.maxNo = $scope.maxNo.value;
				$scope.model.value.index=$scope.index.value;
				
			}
            else{
				//pre populate from model
			}
        });